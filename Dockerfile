FROM openhab/openhab:amd64
VOLUME /openhab/addons

# copy isy plugin
COPY org.openhab.binding.isy-2.1.0-SNAPSHOT.jar /openhab/addons/

# run a do-nothing command
CMD ["true"]

